package Arrays;

public class CopyElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
           int arr[]=new int[]{1,2,3,4,5,6};
           int arr1[]=new int[arr.length];
           for(int i=0;i<arr.length;i++){
        	   arr1[i]=arr[i];
        	   
           }
           System.out.println("Elements in original array");
           for(int i=0;i<arr.length;i++){
        	   System.out.print(arr[i]+" ");
           }
           System.out.println();
           System.out.println("Elements in copy array");
           for(int i=0;i<arr1.length;i++){
        	   System.out.print(arr1[i]+" ");
           }
	}

}
