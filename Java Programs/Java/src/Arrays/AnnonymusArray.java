package Arrays;

public class AnnonymusArray {
    static void min(int arr[]){
        for(int i=0;i<arr.length;i++){ // Corrected variable name i0 to i
            System.out.println(arr[i]);
        }
    }
    
    public static void main(String[] args){
        min(new int[]{3, 5, 8, 5, 4, 2, 7}); // Moved main method outside min method and corrected array initialization
    }
}
