package BasicPrograms;

import java.util.Scanner;

public class Prime1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number;

		System.out.println("Enter any positive integer : ");
		number = sc.nextInt();

		boolean flag = true;

		for (int i = 2; i < number; i++) {
			if (number % i == 0) {

				flag = false;
				break;

			}
		}
		if (flag && number > 1) {
			System.out.println("Number is prime");
		} else {
			System.out.println("Number is not prime");
		}

	}

}
