package BasicPrograms;

public class ConvertFromString {
    public static void main(String[] args) {
       
        String stringToInt = "42";
        int myInt = Integer.parseInt(stringToInt);

        
        String stringToFloat = "3.14";
        float myFloat = Float.parseFloat(stringToFloat);

       
        String stringToChar = "Hello";
        char myChar = stringToChar.charAt(0); 

        
        System.out.println("String to int: " + myInt);
        System.out.println("String to float: " + myFloat);
        System.out.println("String to char: " + myChar);
    }
}
