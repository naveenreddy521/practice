package BasicPrograms;

import java.util.Scanner;

public class PythagoreanTriplet {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int L = scanner.nextInt();
		int count = 0;

		for (int a = 1; a <= L; a++) {
			for (int b = a + 1; b <= L; b++) {
				int cSquare = a * a + b * b;
				int c = (int) Math.sqrt(cSquare);

				if (c * c == cSquare && c <= L) {
					count++;
				}
			}
		}

		System.out.println("Number of Pythagorean triplets: " + count);
		scanner.close();
	}
}
