package BasicPrograms;

public class Pyramid3 {

	public static void main(String[] args) {
		int rows = 5;

		  for (int i = 0; i < rows; i++) {
	            // Print spaces before the stars
	            for (int j = 0; j < rows - i - 1; j++) {
	                System.out.print(" ");
	            }
	            // Print stars
	            for (int j = 0; j < 2 * i + 1; j++) {
	                System.out.print("*");
	            }
	            // Move to the next line
	            System.out.println();
	        }

	}

}
