package BasicPrograms;

import java.util.Scanner;

public class Product2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter m and n values :");

		int m = scanner.nextInt();
		int n = scanner.nextInt();

		long product = 1;
		for (int i = m; i <= n; i++) {
			product *= i;
		}

		System.out.println(product);
	}
}
