package BasicPrograms;

import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the No of Sides :");
        int sides = scanner.nextInt();
        
        switch (sides) {
            case 3:
                System.out.println("Triangle");
                break;
            case 4:
                System.out.println("Quadrilateral");
                break;
            case 5:
                System.out.println("Pentagon");
                break;
            case 6:
                System.out.println("Hexagon");
                break;
            case 7:
            	System.out.println("Septagon");
            	break;
            default:
                System.out.println("Polygon");
                break;
        }
    }
}
