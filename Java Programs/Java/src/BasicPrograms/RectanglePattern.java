package BasicPrograms;

import java.util.Scanner;

public class RectanglePattern {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number");
        int M = sc.nextInt();
        int N = sc.nextInt();

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                System.out.print("+ ");
            }
            System.out.println(); 
        }
    }
}
