package BasicPrograms;

import java.util.Scanner;

public class Power {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter N and M : ");

		int n = scanner.nextInt();
		int m = scanner.nextInt();

		int result = 1;
		for (int i = 0; i < m; i++) {
			result *= n;
		}
		System.out.println(result);
	}
}
