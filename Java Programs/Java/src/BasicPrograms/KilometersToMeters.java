package BasicPrograms;

import java.util.Scanner;

public class KilometersToMeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        
        System.out.print("Enter the number of kilometers: ");
        double kilometers = scanner.nextDouble();
        
        
        int meters = (int) (kilometers * 1000);
        
      
        System.out.println("The equivalent distance in meters is: " + meters);
        
       
        scanner.close();
    }
}
