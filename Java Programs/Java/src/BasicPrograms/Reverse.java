package BasicPrograms;

import java.util.Scanner;

public class Reverse
{
    public static void main(String[] args)
    {
        Scanner sc= new Scanner(System.in);
     
        int number;
        int reverse = 0;
        
        System.out.print("Enter the number ");
        number = sc.nextInt();
        
        int temp = number;
        int rem = 0;
        
        while(temp>0)
        {
	    rem = temp % 10;
	    reverse = reverse * 10 + rem;
            temp =temp/10;
        }

        System.out.println("Reverse of a" + number + " is " + reverse);
    }
}
