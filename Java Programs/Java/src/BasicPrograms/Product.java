package BasicPrograms;

import java.util.Scanner;

public class Product {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter N value : ");
        int N = scanner.nextInt();
        long product = 1; 

        for (int i = 0; i < N; i++) {
            int num = scanner.nextInt();
            product *= num; 
        }

        System.out.println(product); 
    }
}
