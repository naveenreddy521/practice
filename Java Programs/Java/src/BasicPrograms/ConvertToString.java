package BasicPrograms;

public class ConvertToString {
    public static void main(String[] args) {
        
        char myChar = 'a';
        String charToString1 = String.valueOf(myChar);
        String charToString2 = myChar + "";

        
        int myInt = 42;
        String intToString1 = String.valueOf(myInt);
        String intToString2 = Integer.toString(myInt);

        
        float myFloat = 3.14f;
        String floatToString1 = String.valueOf(myFloat);
        String floatToString2 = Float.toString(myFloat);

        
        System.out.println("Char to String:");
        System.out.println("String from char using String.valueOf(): " + charToString1);
        System.out.println("String from char using concatenation: " + charToString2);
        System.out.println();

        System.out.println("Int to String:");
        System.out.println("String from int using String.valueOf(): " + intToString1);
        System.out.println("String from int using Integer.toString(): " + intToString2);
        System.out.println();

        System.out.println("Float to String:");
        System.out.println("String from float using String.valueOf(): " + floatToString1);
        System.out.println("String from float using Float.toString(): " + floatToString2);
    }
}
