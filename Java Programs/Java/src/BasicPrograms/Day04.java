package BasicPrograms;

import java.util.Scanner;

public class Day04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number and day : ");

		String firstDayOfWeek = scanner.nextLine();

		int dayOfMonth = scanner.nextInt();

		String[] daysOfWeek = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

		int firstDayIndex = 0;
		for (int i = 0; i < daysOfWeek.length; i++) {
			if (daysOfWeek[i].equalsIgnoreCase(firstDayOfWeek)) {
				firstDayIndex = i;
				break;
			}
		}

		int dayOfWeekIndex = (firstDayIndex + dayOfMonth - 1) % 7;

		System.out.println(daysOfWeek[dayOfWeekIndex]);

		
	}
}
