package BasicPrograms;

import java.util.Scanner;

public class PowerDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int power;
		int base;
		int result = 1;

		System.out.println("Enter the power :");
		power = sc.nextInt();

		System.out.println("Enetr the base : ");
		base = sc.nextInt();

		for (int i = 1; i <= power; i++) {
			result *= base;
		}

		System.out.println(result);

	}

}
