package BasicPrograms;

import java.util.Scanner;

public class DaysConverter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the no of days :");
        
        int totalDays = scanner.nextInt();
        
        
        int years = totalDays / 365;
        int remainingDays = totalDays % 365;
        int weeks = remainingDays / 7;
        remainingDays %= 7;
        
        
        System.out.println(years + " Y");
        System.out.println(weeks + " W");
        System.out.println(remainingDays + " D");
        
        
        scanner.close();
    }
}
