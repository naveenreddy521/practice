package BasicPrograms;

public class Ternery {
	public static void main(String[] args) {
		int x = 18;
		int y = 19;

		int max = (x > y) ? x : y;

		String result = (x % 2 == 0) ? "Even" : "Odd";
		String result1 =(y % 2 ==0)? "Even" :"Odd";

		System.out.println("Maximum value: " + max);
		System.out.println("x is " + result);
		System.out.println("y is " + result1);
	}
}
