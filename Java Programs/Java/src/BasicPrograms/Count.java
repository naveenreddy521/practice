package BasicPrograms;

import java.util.Scanner;

public class Count {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the n value :");
		
		int n = scanner.nextInt();

		
		int count = 0;
		for (int i = 1; i <= n; i++) {
			int num = i;
			while (num > 0) {
				count++;
				num /= 10;
			}
		}

	
		System.out.println(count);
	}
}
