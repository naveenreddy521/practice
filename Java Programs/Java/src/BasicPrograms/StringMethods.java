package BasicPrograms;

public class StringMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s="  Naveen";
		   System.out.println(s.toUpperCase());
		   System.out.println(s.toLowerCase());
		   System.out.println(s.trim());
		   System.out.println(s.startsWith("a"));
		   System.out.println(s.endsWith("n"));
		   System.out.println(s.charAt(0));   
		   System.out.println(s.charAt(3));
		   System.out.println(s.length());  
		   String s1="Java is a programming language";      
		   String replaceString=s1.replace("Java","python");      
		   System.out.println(replaceString);  
		   
		   
		   String str1 = "Hello";  
	        String str2 = " Naveen";  
	        String str3 = " Reddy";  
	           
	        String str4 = str1.concat(str2);          
	        System.out.println(str4);  
	        
	        String str5 = str1.concat(str2).concat(str3);  
	        System.out.println(str5);
	        
	        String s11="Naveen";  
	        String s2="Naveen";  
	        String s3="Reddy";  
	        String s4="Nani";  
	        System.out.println(s11.equalsIgnoreCase(s2));  
	        System.out.println(s11.equalsIgnoreCase(s3));
	        System.out.println(s11.equalsIgnoreCase(s4));
	        
	        System.out.println(s.substring(2,4));
		  
	}

}
