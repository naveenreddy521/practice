package Pattern;

import java.util.Scanner;

public class NumberPattern2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
          Scanner sc=new Scanner(System.in);
          System.out.println("Enter N value : ");
          int n=sc.nextInt();
          for(int i=1,p=5;i<=n;i++,p--){
        	  for(int j=1;j<=i;j++){
        		  System.out.print("  ");
        	  }
        	  for(int j=i;j<=n;j++){
        		  System.out.print(p+ " ");
        	  }
        	  System.out.println();
          }
	}

}
