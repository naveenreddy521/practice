package Pattern;

public class DiamondNumber {

    public static void main(String[] args) {
        int n = 5;
        
        // Upper half of the diamond
        for (int i = 1, p = 1; i <= n; i++, p++) {
            for (int j = i; j < n; j++) {
                System.out.print("  ");
            }
            for (int j = 1; j < i; j++) {
                System.out.print(p + " ");
            }
            for (int j = 1; j <= i; j++) {
                System.out.print(p + " ");
            }
            System.out.println();
        }

        // Lower half of the diamond
        for (int i = 1, p = n - 1; i < n; i++, p--) {
            for (int j = 1; j <= i; j++) {
                System.out.print("  ");
            }
            for (int j = i; j < n - 1; j++) {
                System.out.print(p + " ");
            }
            for (int j = i; j <= n - 1; j++) {
                System.out.print(p + " ");
            }
            System.out.println();
        }
    }
}
