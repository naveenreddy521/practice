package Collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListToArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
            List<String> list=new ArrayList<String>();
            list.add("Nani");
            list.add("Naveen");
            list.add("Reddy");
            
            String[]array=list.toArray (new String[list.size()]);
            
            System.out.println("Printing Array: "+Arrays.toString(array));  
            System.out.println("Printing List: "+list);
            
	}

}
