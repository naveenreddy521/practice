package Collections;

import java.util.LinkedList;

public class LinkedListDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> ll = new LinkedList<String>();
		ll.add("Naveen");
		ll.add("Reddy");
		ll.add("Kumar");
		ll.add("Hyderabad");

		System.out.println(ll);

		ll.add(3, "KPHB");

		System.out.println(ll);

		LinkedList<String> ll1 = new LinkedList<String>();

		ll1.add("Sai");
		ll1.add("Shiva");
		ll1.add("SIngh");

		ll.addAll(ll1);

		System.out.println(ll);

		ll.addAll(1, ll1);

		System.out.println(ll);

		ll.addFirst("K");

		System.out.println(ll);

		ll.addLast("Stop");

		System.out.println(ll);
	}

}
