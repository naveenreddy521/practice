package Collections;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashSet<String> hs = new HashSet<String>();
		hs.add("Naveen");
		hs.add("Reddy");
		hs.add("Nani");
		hs.add("Hyd");

		Iterator<String> itr = hs.iterator();
		while (itr.hasNext())

			System.out.println(itr.next());
	}

}
