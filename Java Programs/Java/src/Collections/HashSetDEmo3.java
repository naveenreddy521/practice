package Collections;

import java.util.*;

class HashSetDEmo3 {
	public static void main(String args[]) {
		LinkedList<String> list = new LinkedList<String>();
		list.add("Ravi");
		list.add("Vijay");
		list.add("Ajay");

		HashSet<String> set = new HashSet(list);
		set.add("Gaurav");
		Iterator<String> i = set.iterator();
		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}
}
