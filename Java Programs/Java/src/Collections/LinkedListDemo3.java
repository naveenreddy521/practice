package Collections;

import java.util.LinkedList;

public class LinkedListDemo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList<String> ll = new LinkedList<String>();
		ll.add("Naveen");
		ll.add("Reddy");
		ll.add("Kumar");
		ll.add("Hyderabad");

		System.out.println(ll);

		ll.remove("Kumar");

		System.out.println(ll);

		ll.remove(2);

		System.out.println(ll);

		LinkedList<String> ll1 = new LinkedList<String>();

		ll1.add("Sai");
		ll1.add("Shiva");
		ll1.add("SIngh");

		System.out.println(ll1);

		ll.removeAll(ll1);
		System.out.println(ll);

		ll.add("Sathupally");

		System.out.println(ll);

		ll1.removeFirst();

		System.out.println(ll1);

		ll.removeFirstOccurrence("Sathupally");

		System.out.println(ll);

		ll1.removeLast();
		System.out.println(ll1);

		ll1.removeLastOccurrence("Shiva");

		System.out.println(ll1);

		ll.clear();

		System.out.println(ll);

	}

}
