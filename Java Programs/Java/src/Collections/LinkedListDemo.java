package Collections;

import java.util.Iterator;
import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        LinkedList <String> ll=new LinkedList<String>();		
        ll.add("Naveen");
        ll.add("Reddy");
        ll.add("Hyderabad");
        
        Iterator<String> itr=ll.iterator();
        
        while(itr.hasNext()){
        	System.out.println(itr.next());
        }
	}

}
