package Interviewpractice;

import java.util.Scanner;

class Bs {

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter the number of elements in the array:");
		int n = scanner.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter " + n + " elements in sorted order:");
		for (int i = 0; i < n; i++) {
			arr[i] = scanner.nextInt();
		}

		System.out.println("Enter the key to search:");
		int key = scanner.nextInt();

		int last = arr.length - 1;
		binarySearch(arr, 0, last, key);

		scanner.close();
	}

	public static void binarySearch(int arr[], int iow, int high, int key) {
		int mid = (iow + high) / 2;
		while (iow <= high) {
			if (arr[mid] < key) {
				iow = mid + 1;
			} else if (arr[mid] == key) {
				System.out.println("Element is found at index: " + mid);
				return; 
			} else {
				high = mid - 1;
			}
			mid = (iow + high) / 2;
		}
		System.out.println("Element is not found!");
	}
}
