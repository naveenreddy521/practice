package Interviewpractice;

import java.util.Scanner;

public class Swap2num {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println(" Enter a and b : ");

		int a = sc.nextInt();
		int b = sc.nextInt();

		a = a + b;
		b = a - b;
		a = a - b;

		System.out.println("After swapping a is " + a + "  after swapping b is " + b);
	}

}
