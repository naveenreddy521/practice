package Interviewpractice;

import java.util.Scanner;

public class CheckPalindroneString {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter a string to check if it's a palindrome: ");
		String str = scanner.nextLine();

		boolean isPalindrome = checkPalindromeString(str);

		if (isPalindrome) {
			System.out.println("The string \"" + str + "\" is a palindrome.");
		} else {
			System.out.println("The string \"" + str + "\" is not a palindrome.");
		}

		scanner.close();
	}

	public static boolean checkPalindromeString(String input) {
		boolean result = true;
		int length = input.length();

		for (int i = 0; i < length / 2; i++) {
			if (input.charAt(i) != input.charAt(length - i - 1)) {
				result = false;
				break;
			}
		}

		return result;
	}
}
