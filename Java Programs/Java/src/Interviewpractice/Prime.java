package Interviewpractice;
import java.util.Scanner;

public class Prime {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the number : ");

		int num = sc.nextInt();

		System.out.println("Result :" + isPrime(num));

	}

	public static boolean isPrime(int num) {

		if (num == 0 || num == 1) {
			return false;
		}
		if (num == 2) {
			return true;
		}
		for (int i = 2; i <= num/2; i++) {
			if (num % i == 0) {
				return false;
			}
		}

		return true;
	}

}
