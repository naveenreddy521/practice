package Interviewpractice;

import java.util.Scanner;

public class StringContainsVowel {

	public static void main(String[] args) {    
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string :");

		String str = sc.next();

		boolean containsVowels = stringContainsVowels(str);
		System.out.println("The string \"" + str + "\" contains vowels: " + containsVowels);

	}

	public static boolean stringContainsVowels(String str) {
		return str.toLowerCase().matches(".*[aeiou].*");
	}

}
