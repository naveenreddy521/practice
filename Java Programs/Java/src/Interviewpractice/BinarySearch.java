package Interviewpractice;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter the size of the array : ");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter the " + size + " of the sorted integers : ");

		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}
		Arrays.sort(arr);
	
		System.out.println("enter the key to search ");
		int key = sc.nextInt();

		int result = binarySearch(arr, key, 0, size - 1);
		if (result == -1) {
			System.out.println("key not found in array");
		} else {
			System.out.println("key found at index : " + result);
		}
	}

	public static int binarySearch(int[] arr, int key, int low, int high) {
		// TODO Auto-generated method stub
		int mid = (low + high) / 2;

		while (low <= high) {
			if (arr[mid] < key) {
				low = mid + 1;
			} else if (arr[mid] == key) {
				return mid;
			} else {
				high = mid - 1;
			}
			mid = (low + high) / 2;

		}
		return -1;
	}

}
