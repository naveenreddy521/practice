package Interviewpractice;

public class ExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Dog dog = null;

		try {
			System.out.println(dog.dogName);
		} catch (NullPointerException e) {
			dog = new Dog();
			System.out.println(dog.dogName);
		}
	}

}
