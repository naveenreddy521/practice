package Interviewpractice;

import java.util.Scanner;

public class CheckOdd {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter the number of integers you will input: ");
		int count = scanner.nextInt();

		int[] numbers = new int[count];
		System.out.println("Enter the integers:");

		for (int i = 0; i < count; i++) {
			numbers[i] = scanner.nextInt();
		}

		boolean allOdd = onlyOddNumbers(numbers);
		System.out.println("All numbers are odd: " + allOdd);
	}

	public static boolean onlyOddNumbers(int[] numbers) {
		for (int number : numbers) {
			if (number % 2 == 0) {
				return false;
			}
		}
		return true;
	}
}
