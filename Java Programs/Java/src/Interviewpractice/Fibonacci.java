package Interviewpractice;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter the number of terms for the Fibonacci sequence: ");
		int count = scanner.nextInt();

		printFibonacciSequence(count);
	}

	public static void printFibonacciSequence(int count) {
		int a = 0;
		int b = 1;
		int c = 1;

		for (int i = 1; i <= count; i++) {
			System.out.print(a + (i < count ? ", " : ""));
			a = b;
			b = c;
			c = a + b;
		}
	}
}
