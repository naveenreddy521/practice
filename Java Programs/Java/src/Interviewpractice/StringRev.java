package Interviewpractice;

import java.util.Scanner;

public class StringRev {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the string :");

		String str = sc.next();

		sc.close();

		System.out.println("Reversed string  :" + reverse(str));
	}

	public static String reverse(String s) {

		StringBuilder sb = new StringBuilder();

		char[] ch = s.toCharArray();

		for (int i = ch.length - 1; i >= 0; i--) {
			sb.append(ch[i]);
		}
		return sb.toString();

	}

}
