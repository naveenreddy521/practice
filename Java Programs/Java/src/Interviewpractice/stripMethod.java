package Interviewpractice;

import java.util.Scanner;

public class stripMethod {
    public static void main(String[] args) {
        // Create a Scanner object to read input from the user
        Scanner scanner = new Scanner(System.in);
        
        // Prompt the user to enter a string
        System.out.println("Enter a string:");
        String userInput = scanner.nextLine();
        
        // Apply the strip method to remove leading and trailing whitespace
        String strippedInput = userInput.trim();
        
        // Print the result
        System.out.println("String after stripping leading and trailing whitespaces: " + strippedInput);
        
        // Close the scanner to prevent resource leaks
        scanner.close();
    }
}
