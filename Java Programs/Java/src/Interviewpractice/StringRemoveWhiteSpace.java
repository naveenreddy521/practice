package Interviewpractice;

import java.util.Scanner;

public class StringRemoveWhiteSpace {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the string : ");

		String str = sc.nextLine();

		String result = removeWhiteSpaces(str);
		
		System.out.println("string after removing white spaces :" + result);

	}

	public static String removeWhiteSpaces(String str) {
		StringBuilder sb = new StringBuilder();
		char[] ch = str.toCharArray();

		for (char c : ch) {
			if (!Character.isWhitespace(c)) {
				sb.append(c);
			}

		}
		return sb.toString();
	}

}
