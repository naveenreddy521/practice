class Vehicle {
    String brand = "Hero";
    
    void displayBrand() {
        System.out.println(brand);
    }
}

class Car extends Vehicle {
    String modelName = "Innova";
}

public class Inheritance {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.displayBrand();
        System.out.println(myCar.brand + " " + myCar.modelName);
    }
}
