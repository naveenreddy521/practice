 class Car {
    String color;
    String model;

    Car(String color,String model){
        this.color=color;
        this.model=model;
    }

    void displayDetails(){
          System.out.print(model);
          System.out.println(color);
    }
}
public class Main{
        public static void main(String[] args) {
            Car car1 = new Car("Red", "swift");
            car1.displayDetails();
        }
}
