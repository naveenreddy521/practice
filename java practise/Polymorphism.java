class Animal{
    void sound(){
        System.out.println("animal makes sound");
    }
}

class Dog extends Animal{
    void sound(){
        System.out.println("Dog barks");
    }
}
class Cat extends Animal{
    void sound(){
System.out.println("Cat sounds");
    }
}



public class Polymorphism {
    public static void main(String[] args) {
        Animal dg=new Dog();
        Animal ct=new Cat();

        dg.sound();
        ct.sound();

    }
}
