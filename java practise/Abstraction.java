abstract class Shape{
    private String color;

    abstract void draw();
       void setColor(String color){
        this.color=color;
       }

       String getColor(){
        return color;
       }
    }
    class Circle extends Shape{
        void draw(){
            System.out.println("circle draws");
        }
    }
    class Rectangle extends Shape{
        void draw(){
            System.out.println("Rectangle draws");
        }
    }

    public class Abstraction{
        public static void main(String[] args) {
            Shape myCircle = new Circle();
            Shape myRectangle = new Rectangle();

            myCircle.setColor("RED");
            myRectangle.setColor("BLUE");

            myCircle.draw();
            System.out.println("circle color " + myCircle.getColor());
            myRectangle.draw();

            System.out.println("rectangle color " + myRectangle.getColor());
        }
    }
